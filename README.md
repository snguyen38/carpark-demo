# Getting Started

### Note: 
  - Please install Docker first to run this app locally
  - Java8 and Maven are required to run the build

### Local setup


* Step 1: Build custom docker image for Postgres DB with postgis extension
  
  Note: Needed postgis for calculate distance on the query to find the nearest carpark 

    Open command promt and direct to docker/postgres then run command below

    `docker build -t postgres-with-postgis .`


* Step 2: Build the app

    Run command below on main folder to build a jar file and running unit tests along with the build

    `mvn clean package`


* Step 3: Start the app
  
    Run command below on main folder to start the Postgres DB as well as the App.

    `docker-compose up`

    There are some Automation jobs run on this step included:
  * Enable Postgis extension
  * Create 2 table: carpark_info, carpark_availability
  * Publish carpark data from csv file to table carpark_info


* Step 4: Test the API
  
  You can easily test the API by any browser/postman tool with URL below:
  `http://localhost:8080/carparks/nearest?latitude=1.3010632720875&longitude=103.854118049931&page=1&per_page=20`

