create extension postgis;
create extension postgis_topology;
CREATE TABLE carpark_info (
                          car_park_no VARCHAR(255) PRIMARY KEY,
                          address VARCHAR(255),
                          latitude NUMERIC,
                          longitude NUMERIC,
                          car_park_type VARCHAR(255),
                          type_of_parking_system VARCHAR(255),
                          short_term_parking VARCHAR(255),
                          free_parking VARCHAR(255),
                          night_parking VARCHAR(255),
                          car_park_decks INTEGER,
                          gantry_height NUMERIC,
                          car_park_basement VARCHAR(255)
);

-- It may help on larger dataset but with only around 2k rows it not make huge difference :)
-- CREATE INDEX idx_location_calculate_point ON carpark_info USING GIST (ST_MakePoint(longitude, latitude));

COPY carpark_info FROM '/docker-entrypoint-initdb.d/carpark-info.csv' WITH CSV HEADER;


CREATE TABLE carpark_availability (
                                 car_park_no VARCHAR(255) PRIMARY KEY,
                                 total_lots INTEGER,
                                 available_lots INTEGER
);

