package com.example.demo.services;

import java.util.List;

import com.example.demo.models.CarparkLocation;
import org.springframework.stereotype.Service;

@Service
public interface ILocationService {
    List<CarparkLocation> getNearestLocations(double latitude, double longitude, int page, int perPage);
}
