package com.example.demo.services;

import com.example.demo.models.CarparkLocation;
import com.example.demo.dao.ILocationDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService implements ILocationService {

    @Autowired
    private ILocationDAO locationDAO;

    public List<CarparkLocation> getNearestLocations(double latitude, double longitude, int page, int perPage) {
        return locationDAO.getNearestLocations(latitude, longitude, page, perPage);
    }
}
