package com.example.demo.services;


import com.example.demo.models.CarparkAvailability;
import com.example.demo.models.CarparkAvailabilityData;
import com.example.demo.dao.ICarparkAvailabilityDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleJobs {
    private static final Logger LOGGER = LogManager.getLogger(ScheduleJobs.class);
    private RestTemplate restTemplate;
    private ICarparkAvailabilityDAO carparkAvailabilityDao;

    @Value("${carpark.api.endpoint}")
    private String carparkApi;

    public ScheduleJobs(RestTemplate restTemplate, ICarparkAvailabilityDAO carparkAvailabilityDao) {
        this.restTemplate = restTemplate;
        this.carparkAvailabilityDao = carparkAvailabilityDao;
    }

    @Scheduled(fixedDelay = 60000, initialDelay = 1000) // run on 1s after App started and every next minute counting on the last run complete
    public void updateCarparkAvailability() {
        LOGGER.info("Start getting carpark availability data");
        CarparkAvailabilityData carparkAvailabilityData = restTemplate.getForObject(carparkApi, CarparkAvailabilityData.class);
        LOGGER.info("Getting data done");

        try {
            LOGGER.info("start mapping data");
            List<CarparkAvailability> carparkAvailabilities = mappingCarparkDataToListAvailable(carparkAvailabilityData);
            LOGGER.info("end  mapping data");

            LOGGER.info("start update data to DB");
            carparkAvailabilityDao.save(carparkAvailabilities);
            LOGGER.info("Update data to DB done!");
        } catch(NullPointerException e){
            LOGGER.error(e);
        }
    }

    private List<CarparkAvailability> mappingCarparkDataToListAvailable(CarparkAvailabilityData data) {
        return data.getItems().stream()
                .flatMap(item -> item.getCarparkData().stream())
                .map(carparkData -> {
                    String carparkNumber = carparkData.getCarparkNumber();
                    int totalLots = carparkData.getCarparkInfo().get(0).getTotalLots();
                    int lotsAvailable = carparkData.getCarparkInfo().get(0).getLotsAvailable();
                    return new CarparkAvailability(carparkNumber, totalLots, lotsAvailable);
                })
                .collect(Collectors.toList());
    }


}
