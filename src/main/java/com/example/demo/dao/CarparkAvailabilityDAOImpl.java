package com.example.demo.dao;

import com.example.demo.models.CarparkAvailability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CarparkAvailabilityDAOImpl implements ICarparkAvailabilityDAO {

    @Value("${batch.size}")
    private int batchSize;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(List<CarparkAvailability> carparkAvailabilityList) {
        int totalSize = carparkAvailabilityList.size();
        for (int i = 0; i < totalSize; i += batchSize) {
            int endIndex = Math.min(i + batchSize, totalSize);
            List<CarparkAvailability> batch = carparkAvailabilityList.subList(i, endIndex);
            jdbcTemplate.batchUpdate(
                    "INSERT INTO carpark_availability (car_park_no, total_lots, available_lots) " +
                            "VALUES (?, ?, ?) " +
                            "ON CONFLICT (car_park_no) " +
                            "DO UPDATE SET total_lots = ?, available_lots = ?",
                    new BatchPreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement ps, int j) throws SQLException {
                            CarparkAvailability carparkAvailability = batch.get(j);
                            ps.setString(1, carparkAvailability.getCarparkNumber());
                            ps.setInt(2, carparkAvailability.getTotalLots());
                            ps.setInt(3, carparkAvailability.getLotsAvailable());
                            ps.setInt(4, carparkAvailability.getTotalLots());
                            ps.setInt(5, carparkAvailability.getLotsAvailable());
                        }

                        @Override
                        public int getBatchSize() {
                            return batch.size();
                        }
                    });
        }
    }
}
