package com.example.demo.dao;

import java.util.List;

import com.example.demo.models.CarparkAvailability;

public interface ICarparkAvailabilityDAO {
    void save(List<CarparkAvailability> carparkAvailabilityList);
}
