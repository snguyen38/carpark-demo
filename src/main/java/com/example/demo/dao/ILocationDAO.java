package com.example.demo.dao;

import java.util.List;

import com.example.demo.models.CarparkLocation;

public interface ILocationDAO {
    List<CarparkLocation> getNearestLocations(double latitude, double longitude, int page, int perPage);
}
