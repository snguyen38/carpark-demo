package com.example.demo.dao;

import com.example.demo.models.CarparkLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LocationDAOImpl implements ILocationDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<CarparkLocation> getNearestLocations(double latitude, double longitude, int page, int perPage) {
        String sql = "SELECT cp.address, cp.latitude, cp.longitude, cpa.total_lots, cpa.available_lots " +
                "FROM carpark_info cp " +
                "JOIN carpark_availability cpa ON cp.car_park_no = cpa.car_park_no " +
                "WHERE cpa.available_lots > 0 " +
                "ORDER BY ST_Distance(ST_MakePoint(cp.longitude, cp.latitude), ST_MakePoint(?, ?)) " +
                "OFFSET ? LIMIT ?";

        int offset = (page - 1) * perPage;
        return jdbcTemplate.query(sql, new Object[] {longitude, latitude, offset, perPage}, (rs, rowNum) -> {
            CarparkLocation carparkLocation = new CarparkLocation();
            carparkLocation.setAddress(rs.getString("address"));
            carparkLocation.setLatitude(rs.getDouble("latitude"));
            carparkLocation.setLongitude(rs.getDouble("longitude"));
            carparkLocation.setTotalLots(rs.getInt("total_lots"));
            carparkLocation.setAvailableLots(rs.getInt("available_lots"));
            return carparkLocation;
        });
    }
}
