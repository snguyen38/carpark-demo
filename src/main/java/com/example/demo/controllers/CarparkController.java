package com.example.demo.controllers;

import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.example.demo.models.CarparkLocation;
import com.example.demo.services.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
public class CarparkController {
    @Autowired
    private ILocationService locationService;

    @GetMapping("/carparks/nearest")
    public List<CarparkLocation> getNearestLocations(@RequestParam @Min(-90) @Max(90) double latitude,
                                                     @RequestParam @Min(-180) @Max(180) double longitude,
                                                     @RequestParam(required = false, defaultValue = "1") @Min(1) int page,
                                                     @RequestParam(required = false, defaultValue = "10", name = "per_page") @Min(1) @Max(100) int perPage) {
        return locationService.getNearestLocations(latitude, longitude, page, perPage);
    }
}
