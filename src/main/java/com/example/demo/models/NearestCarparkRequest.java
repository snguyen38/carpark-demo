package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class NearestCarparkRequest {
    @NotNull
    private Double latitude;

    @NotNull
    private Double longitude;

    @Min(1)
    private Integer page;

    @Min(1)
    @Max(50)
    @JsonProperty("per_page")
    private Integer perPage;
}
