package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CarparkAvailability {
    private String carparkNumber;
    private int totalLots;
    private int lotsAvailable;
}
