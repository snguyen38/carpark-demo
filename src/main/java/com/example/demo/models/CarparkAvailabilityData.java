package com.example.demo.models;

import lombok.Data;

import java.util.List;

@Data
public class CarparkAvailabilityData {
    private List<CarparkItem> items;
}
