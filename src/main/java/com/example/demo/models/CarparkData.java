package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class CarparkData {
    @JsonProperty("carpark_info")
    private List<CarparkInfo> carparkInfo;
    @JsonProperty("carpark_number")
    private String carparkNumber;
    @JsonProperty("update_datetime")
    private LocalDateTime updateDatetime;
}
