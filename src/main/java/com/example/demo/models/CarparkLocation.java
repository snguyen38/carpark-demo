package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarparkLocation {
    private String address;
    private double latitude;
    private double longitude;
    @JsonProperty("total_lots")
    private int totalLots;
    @JsonProperty("available_lots")
    private int availableLots;
}
