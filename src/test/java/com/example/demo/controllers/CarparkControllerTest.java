package com.example.demo.controllers;


import java.util.Arrays;
import java.util.List;

import com.example.demo.models.CarparkLocation;
import com.example.demo.services.ILocationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CarparkController.class)
@AutoConfigureMockMvc
class CarparkControllerTest {

    @MockBean
    private ILocationService locationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetNearestLocations_ValidInput_ReturnsCarparkLocations() throws Exception {
                List<CarparkLocation> expectedLocations = Arrays.asList(new CarparkLocation(), new CarparkLocation());
                when(locationService.getNearestLocations(anyDouble(), anyDouble(), anyInt(), anyInt()))
                    .thenReturn(expectedLocations);

                mockMvc.perform(get("/carparks/nearest")
                           .param("latitude", "1.0")
                           .param("longitude", "2.0")
                           .param("page", "1")
                           .param("per_page", "10"))
                       .andExpect(status().isOk())
                       .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                       .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    void testGetNearestLocations_InvalidLatitude_ThrowsMethodArgumentNotValidException() throws Exception {
        mockMvc.perform(get("/carparks/nearest")
                   .param("latitude", "91.0")
                   .param("longitude", "2.0")
                   .param("page", "1")
                   .param("per_page", "10"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.errors", hasItem("latitude - must be less than or equal to 90")));
    }

    @Test
    void testGetNearestLocations_InvalidLongitude_ThrowsMethodArgumentNotValidException() throws Exception {
        mockMvc.perform(get("/carparks/nearest")
                   .param("latitude", "1.0")
                   .param("longitude", "181.0")
                   .param("page", "1")
                   .param("per_page", "10"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.errors", hasItem("longitude - must be less than or equal to 180")));
    }

    @Test
    void testGetNearestLocations_InvalidPage_ThrowsMethodArgumentNotValidException() throws Exception {
        mockMvc.perform(get("/carparks/nearest")
                   .param("latitude", "1.0")
                   .param("longitude", "180.0")
                   .param("page", "-1")
                   .param("per_page", "10"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.errors", hasItem("page - must be greater than or equal to 1")));
    }

    @Test
    void testGetNearestLocations_InvalidPerPage_ThrowsMethodArgumentNotValidException() throws Exception {
        mockMvc.perform(get("/carparks/nearest")
                   .param("latitude", "1.0")
                   .param("longitude", "180.0")
                   .param("page", "1")
                   .param("per_page", "120"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.errors", hasItem("perPage - must be less than or equal to 100")));
    }

    @Test
    void testGetNearestLocations_InvalidInputTypeLatitude_ThrowsMethodArgumentTypeMismatchException() throws Exception {
        mockMvc.perform(get("/carparks/nearest")
                   .param("latitude", "1.0aa")
                   .param("longitude", "180.0")
                   .param("page", "1")
                   .param("per_page", "120"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.errors", hasItem("Failed to convert value of type 'java.lang.String' to required type 'double'; nested exception is java.lang.NumberFormatException: For input string: \"1.0aa\"")));
    }

    @Test
    void testGetNearestLocations_InvalidInputType_ThrowsMethodArgumentTypeMismatchException() throws Exception {
        mockMvc.perform(get("/carparks/nearest")
                   .param("latitude", "1.0")
                   .param("longitude", "180.0")
                   .param("page", "1-asd")
                   .param("per_page", "120"))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.errors", hasItem("Failed to convert value of type 'java.lang.String' to required type 'int'; nested exception is java.lang.NumberFormatException: For input string: \"1-asd\"")));
    }

}
