package com.example.demo.services;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.dao.ICarparkAvailabilityDAO;
import com.example.demo.models.CarparkAvailability;
import com.example.demo.models.CarparkAvailabilityData;
import com.example.demo.models.CarparkData;
import com.example.demo.models.CarparkInfo;
import com.example.demo.models.CarparkItem;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ScheduleJobsTest {

    @Test
    void testUpdateCarparkAvailabilitya() throws NoSuchFieldException, IllegalAccessException {
        // create mock objects
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ICarparkAvailabilityDAO carparkAvailabilityDao = Mockito.mock(ICarparkAvailabilityDAO.class);

        // set up mock object responses
        CarparkAvailabilityData carparkAvailabilityData = new CarparkAvailabilityData();
        carparkAvailabilityData.setItems(new ArrayList<>());
        CarparkItem item = new CarparkItem();
        item.setCarparkData(new ArrayList<>());
        CarparkData carparkData = new CarparkData();
        CarparkInfo carparkInfo = new CarparkInfo();
        carparkInfo.setTotalLots(10);
        carparkInfo.setLotsAvailable(5);
        carparkData.setCarparkNumber("C1");
        carparkData.setCarparkInfo(new ArrayList<>());
        carparkData.getCarparkInfo().add(carparkInfo);
        item.getCarparkData().add(carparkData);
        carparkAvailabilityData.getItems().add(item);
        String carparkApiUrl = "http://example.com/api/carpark";

        when(restTemplate.getForObject(carparkApiUrl, CarparkAvailabilityData.class)).thenReturn(carparkAvailabilityData);

        // create ScheduleJobs instance and call updateCarparkAvailability
        ScheduleJobs scheduleJobs = new ScheduleJobs(restTemplate, carparkAvailabilityDao);

        // init private variable using reflection
        Field carparkApiField = ScheduleJobs.class.getDeclaredField("carparkApi");
        carparkApiField.setAccessible(true);
        carparkApiField.set(scheduleJobs, carparkApiUrl);


        scheduleJobs.updateCarparkAvailability();


        // verify that the mock objects were called as expected
        verify(restTemplate, times(1)).getForObject(any(String.class), any(Class.class));
        verify(carparkAvailabilityDao, times(1)).save(any(List.class));

        ArgumentCaptor<List<CarparkAvailability>> captor = ArgumentCaptor.forClass(List.class);
        verify(carparkAvailabilityDao).save(captor.capture());
        assertEquals(1, captor.getValue().size());
        assertEquals(10, captor.getValue().get(0).getTotalLots());
        assertEquals(5, captor.getValue().get(0).getLotsAvailable());
        assertEquals("C1", captor.getValue().get(0).getCarparkNumber());
    }
}
